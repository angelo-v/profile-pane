const path = require('path');
const stencil = require('@stencil/webpack');

module.exports = [{
    mode: 'development',
    plugins: [
        new stencil.StencilPlugin({
            collections: [
                'node-modules/solid-pane-components/dist/solid-pane-components',
            ]
        }),
    ],
    entry: './src/demo.js',
    output: {
        filename: 'demo.bundle.js',
        path: path.resolve(__dirname, 'demo'),
    },
    externals: {
        fs: 'null',
        'node-fetch': 'fetch',
        'isomorphic-fetch': 'fetch',
        xmldom: 'window',
        'text-encoding': 'TextEncoder',
        'whatwg-url': 'window',
        '@trust/webcrypto': 'crypto'
    },
}, {
    mode: 'production',
    plugins: [
        new stencil.StencilPlugin({
            collections: [
                'node-modules/solid-pane-components/dist/solid-pane-components',
            ]
        }),
    ],
    entry: './src/ProfilePane.js',
    output: {
        filename: 'pane.bundle.js',
        path: path.resolve(__dirname, 'dist'),
        libraryTarget: "umd"
    },
    externals: {
        fs: 'null',
        'solid-ui': 'solid-ui',
        'node-fetch': 'fetch',
        'isomorphic-fetch': 'fetch',
        xmldom: 'window',
        'text-encoding': 'TextEncoder',
        'whatwg-url': 'window',
        '@trust/webcrypto': 'crypto'
    },
}];