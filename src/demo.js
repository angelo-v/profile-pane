
import ProfilePane from './ProfilePane';

function component() {
    const element = document.createElement('div');

    element.appendChild(ProfilePane.render());

    return element;
}

document.body.appendChild(component());