
import { icons, ns } from 'solid-ui'

import "solid-pane-components/dist/solid-pane-components";

export default {
    global: false,

    icon: icons.iconBase + 'noun_15059.svg',

    name: 'profile',

    label: function (subject, context) {
        const t = context.session.store.findTypeURIs(subject)
        if (
            t[ns.vcard('Individual').uri] ||
            t[ns.vcard('Organization').uri] ||
            t[ns.foaf('Person').uri] ||
            t[ns.schema('Person').uri]
        ) {
            return 'Profile'
        }
        return null
    },

    render: (subject, context) => {
        const element = document.createElement('profile-view');
        element.profile = {
            uri: "https://jane.doe.example/profile/card#me",
            name: "Jane Doe",
            location: "Hamburg, Germany",
            role: "Software Developer",
            imageSrc: "https://randomuser.me/api/portraits/women/44.jpg",
            note: "I am a mockup - and proud of it.",
            homepage: "https://jane.doe.example/"
        };
        return element;
    }
}
